package org.antwalk;

import org.antwalk.customers.Customer;
import org.antwalk.customers.CustomerCurrent;
import org.antwalk.customers.CustomerSavings;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test
{

	public static void main(String[] args)
	{
		System.out.println("Account Opening\n");
		AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
		Customer c=context.getBean(Customer.class);
		c.display();
		if(c.getChoice().equals("Savings"))
		{
			CustomerSavings cs=context.getBean(CustomerSavings.class);
			System.out.println("Type of Account is:"+cs.ac.showAccount());
			System.out.println("\nAccount is Created for you");
		}
		else if(c.getChoice().equals("Current"))
		{
			CustomerCurrent cs=context.getBean(CustomerCurrent.class);
			System.out.println("Type of Account is:"+cs.ac.showAccount());
			System.out.println("\nAccount is Created for you");
		}
		else
		{
			System.out.println("\nInvalid Input\n");
		}
		context.close();
	}

}
